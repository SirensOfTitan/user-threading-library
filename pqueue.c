#include "pqueue.h"
#include "line.h"

/**
 * Based on implementation at:
 * http://andreinc.net/2011/06/01/implementing-a-generic-priority-queue-in-c/
 */

#define LEFT(x) (2*(x) + 1)
#define RIGHT(x) (2*(x) + 2)
#define PARENT(x) ((x) / 2)

void PQueue_Heapify(PQueue *ref, size_t index);

PQueue *PQueue_New(int (*cmp)(void *p1, void *p2), size_t capacity)
{
	PQueue *ref = NULL;
	ref = malloc(sizeof(PQueue));

	if (ref == NULL)
	{
		perror("Malloc allocation failed");;
		return NULL;
	}

	ref->cmp = cmp;
	ref->capacity = capacity;
	ref->data = NULL;
	ref->data = malloc(capacity * sizeof(*(ref->data)));

	if (ref->data == NULL)
	{
		perror("Malloc allocation failed");
	}

	ref->size = 0;

	return ref;
}

void PQueue_Delete(PQueue *ref)
{
	if (ref == NULL)
		return;

	free(ref->data);
	free(ref);
}

void PQueue_Enqueue(PQueue *q, void *data)
{
	if (q == NULL)
		return;

	size_t i;
	void *tmp = NULL;

	if (q->size >= q->capacity)
	{
		return;
	}

	q->data[q->size] = data;
	i = q->size;
	q->size++;

	while (i > 0 && q->cmp(q->data[i], q->data[PARENT(i)])
		> 0)
	{
		tmp = q->data[i];
		q->data[i] = q->data[PARENT(i)];
		q->data[PARENT(i)] = tmp;
		i = PARENT(i);
	}

	

}

void *PQueue_Dequeue(PQueue *ref)
{
	void *data = NULL;

	if (ref == NULL)
		return NULL;

	if (ref->size < 1)
		return NULL;

	data = ref->data[0];
	ref->data[0] = ref->data[ref->size - 1];
	ref->size--;

	PQueue_Heapify(ref, 0);
	return data;

}

void PQueue_Heapify(PQueue *ref, size_t index)
{
	void *tmp = NULL;

	if (ref == NULL)
		return;

	size_t leftind, rightind, lrgind;

	leftind = LEFT(index);
	rightind = RIGHT(index);

	if (leftind < ref->size && 
		ref->cmp(ref->data[leftind], ref->data[rightind]) 
		> 0)
	{
		lrgind = leftind;
	}
	else
	{
		lrgind = index;
	}

	if (rightind < ref->size &&
		ref->cmp(ref->data[rightind], ref->data[lrgind])
		> 0)
	{
		lrgind = rightind;
	}

	if (lrgind != index)
	{
		tmp = ref->data[lrgind];
		ref->data[lrgind] = ref->data[index];
		ref->data[index] = tmp;

		PQueue_Heapify(ref, lrgind);
	}

}
