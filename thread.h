/**
 * Author: Cole Potrocky
 */
 
#ifndef THREAD_H
#define THREAD_H 1
#include <stdlib.h>
#include <setjmp.h>
#include <signal.h>
#include <time.h>

#define RUNNING 1
#define READY 2
#define SLEEPING 4
#define SUSPENDED 8
#define DELETED 16
#define FINISHED 32
#define BLOCKED 64 

#define TIME_QUANTUM 1
#define MAX_NO_OF_THREADS 100
#define STACK_SIZE 4096
#define TICKET_SAMPLE_SIZE 1000

typedef unsigned long address_t;

typedef struct
{
    int thread_id;
    int status;
    void *function;
    void *functionArg;
    int hasArgs;
    void *returnVal;
    char *stack;
    sigjmp_buf jbuf;
    clock_t recent_startTime;
    clock_t totalTime;
    int numOfBursts;
    int totalRequestedSleepTime;
    clock_t startSleepTime;
    int secondsToSleep;
#ifdef LOTTERY
    int tickets;
#endif
} thread;

typedef struct
{
    int thread_id;
    int current_status;
    clock_t totalTime;
    int totalRequestedSleepTime;
} status;

address_t GetReturnAddress();
void resetTimer();
int CreateThread(void (*f)(void));
int CreateThreadWithArgs(void *(*f)(void *), void *arg);
void Go();
void ToggleBlockThread(int, int);
#ifdef LOTTERY
void SetNumberOfTickets(int, int);
#endif
void UpdateSleepingThreads(void);
int ScheduleThreads();
int GetMyId();
int DeleteThread(int thread_id);
void Dispatch(int);
void YieldCPU();
int SuspendThread(int thread_id);
int ResumeThread(int thread_id);
//int GetStatus(int thread_id, status *stat);
void SleepThread(int sec);
void CleanUp();
#endif

