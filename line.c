/**
 * Author: Cole Potrocky
 */
 
#include <stdlib.h>
#include "line.h"
#include "pqueue.h"
#include "thread.h"
#include "semaphore.h"

int istNum = 0;
int cseNum = 0; 

Semaphore p;

int compare(void *p1, void *p2)
{
	Person *t1 = (Person *)p1;
	Person *t2 = (Person *)p2;

	if (t1->type == t2->type)
	{
		if (t1->status == t2->status)
		{
			if (t1->dob < t2->dob)
			{
				return 1;
			}
			else if (t2->dob < t1->dob)
			{
				return -1;
			}
		}
		else
		{
			if (t1->status > t2->status)
				return 1;
			else if (t2->status > t1->status)
				return -1;

			return 0;
		}

		return 0;
	}
	else
	{
		if (t1->type == IST_TYPE)
		{
			if (cseNum >= istNum)
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			if (cseNum >= istNum)
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}
	}

	// IST is special case.
	return 0;

}

void setupPerson(Person *ref, int type, int status,
	int thread_id)
{
	srand(clock());
	ref->type = type;
	ref->status = status;
	ref->thread_id = thread_id;
	ref->dob = rand() % DOB;
}

int getWaitingTime()
{
	srand(clock());
	return (rand() % MAX_WAIT_TIME);
}

void stroll()
{
	int toWhen = getWaitingTime();
	for (int i = 0; i < toWhen; i++);
}

void cseFaculty()
{
	Person t;
	setupPerson(&t, CMP_TYPE, FAC, GetMyId());

	while (1)
	{
		printf("\n----------------\n");
		printf("CSE Faculty %d: Walking to Critical Path\n", t.thread_id);
		stroll();
		printf("CSE Faculty %d: At Critical Path\n", t.thread_id);

		Semaphore_Wait(&p, &t);
		printf("CSE Faculty %d: In Critical Path\n", t.thread_id);
		stroll();
		Semaphore_Signal(&p);
		printf("CSE Faculty %d: Exited Critical Path\n", t.thread_id);
		printf("*******************\n");
	}
}

void cseStudent()
{
	Person t;
	setupPerson(&t, CMP_TYPE, STUD, GetMyId());

	while (1)
	{
		printf("\n----------------\n");
		printf("CSE Student %d: Walking to Critical Path\n", t.thread_id);
		stroll();
		printf("CSE Student %d: At Critical Path\n", t.thread_id);

		Semaphore_Wait(&p, &t);
		printf("CSE Student %d: In Critical Path\n", t.thread_id);
		stroll();
		Semaphore_Signal(&p);
		printf("CSE Student %d: Exited Critical Path\n", t.thread_id);
		printf("*******************\n");
	}
}

void istPerson()
{
	Person t;
	setupPerson(&t, IST_TYPE, STUD, GetMyId());

	while(1)
	{
		printf("\n----------------\n");
		printf("IST %d: Walking to Critical Path\n", t.dob);
		stroll();
		printf("IST %d: At Critical Path\n", t.dob);

		Semaphore_Wait(&p, &t);
		printf("IST %d: In Critical Path\n", t.dob);
		stroll();
		Semaphore_Signal(&p);
		printf("IST %d: Exited Critical Path\n", t.dob);
		printf("*******************\n");
	}
}

int getISTNum()
{
	return istNum;
}

int getCSENum()
{
	return cseNum;
}

void setISTNum(int new)
{
	istNum = new;
}

void setCSENum(int new)
{
	cseNum = new;
}

void main()
{
	// Only one person can be in the 
	// critical path at a time, so
	// initialize semaphore value to 1.
	Semaphore_Init(&p, 1);

	CreateThread(istPerson);
	CreateThread(istPerson);
	CreateThread(cseStudent);
	CreateThread(cseStudent);
	CreateThread(cseFaculty);

	Go();
}
