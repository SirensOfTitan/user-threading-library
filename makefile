CC = gcc
CFLAGS = -std=gnu99 -g
OBJECTS = thread.o queue.o lock.o semaphore.o pqueue.o line.o

all: $(OBJECTS)
	$(CC) $(CFLAGS) -o threading $(OBJECTS)

line.o: line.c line.h
	$(CC) $(CFLAGS) -c -o line.o line.c

driver.o: driver.c
	$(CC) $(CFLAGS) -c -o driver.o driver.c

queue.o: queue.h queue.c
	$(CC) $(CFLAGS) -c -o queue.o queue.c

thread.o: thread.h thread.c
	$(CC) $(CFLAGS) -c -o thread.o thread.c

lock.o: lock.h lock.c
	$(CC) $(CFLAGS) -c -o lock.o lock.c

semaphore.o: semaphore.h semaphore.c
	$(CC) $(CFLAGS) -c -o semaphore.o semaphore.c

pqueue.o: pqueue.h pqueue.c
	$(CC) $(CFLAGS) -c -o pqueue.o pqueue.c

clean:
	rm *.o threading
