/**
 * Author: Cole Potrocky
 */

#include <stdlib.h>
#include <stdio.h>
#include "thread.h"
#include "lock.h"
#include <stdbool.h>

void Mutex_Lock(mutex *locked)
{
	/**
	 * GNU/gcc test and set atomic builtin.
	 * used to ensure mutex atomicity. 
	 * 
	 * Writes 1 into locked and returns
	 * previous value of locked.
	 */
	while (__sync_lock_test_and_set(locked, 1));
}

void Mutex_Unlock(mutex *locked)
{
	/**
	 * Issues a full memory barrier.
	 */ 
	__sync_synchronize();
	*locked = 0;
}
