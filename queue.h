/**
 * Author: Cole Potrocky
 */
#ifndef QUEUE
#define QUEUE

#include <stdlib.h>
#include <stdio.h>
#include "thread.h"

typedef struct node
{
    
    void *value;
    struct node *next;
} Node;

typedef struct queue
{
    Node *head;
    Node *tail;
    int size;
} Queue;

void Queue_AddElement(Queue *, void *);
void Queue_RemoveElement(Queue *);
void Queue_RemoveElementById(Queue *, int thread_id);
int Queue_CheckForPtr(Queue *, thread *);
#endif

