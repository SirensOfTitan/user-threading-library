/**
 * Author: Cole Potrocky
 */

#include <stdlib.h>
#include <stdio.h>
#include "queue.h"
#include "thread.h"

void Queue_AddElement(Queue * q, void *t)
{
    Node *node = malloc(sizeof(Node));
    
    if (node == NULL)
    {
        perror("Malloc failled");
        return;
    }
    
    node->value = t;
    node->next = NULL;
    
    if (q == NULL)
    // Queue is not initialized
    {
        perror("Queue not initialized");
        free(node);
        return; 
    }
    else if (Queue_CheckForPtr(q, t) == 1)
    // Value is already in Queue.
    {
        return;
    }
    else if (q->head == NULL && q->tail == NULL)
    {
        q->head = q->tail = node;
        q->size = 1;
        return;
    }
    else if (q->head == NULL || q->tail == NULL)
    {
        free(node);
        return;
    }
    else
    {
        q->tail->next = node;
        q->tail = node;
        q->size++;
    }
}

int Queue_CheckForPtr(Queue *q, thread *val)
{
    Node *head = q->head;

    while (head != NULL)
    {
        if (((thread *)head->value)->thread_id == val->thread_id)
            return 1;

        head = head->next;
    }

    return 0;
}

void Queue_RemoveElement(Queue *q)
{
    Node *head = NULL;
    Node *elem = NULL;
    
    if (q == NULL)
    {
        return;
    }
    else if (q->head == NULL && q->tail == NULL)
    {
        return;
    }
    else if (q->head == NULL || q->tail == NULL)
    {
        return;
    }
    
    head = q->head;
    elem = head->next;
    free(head);
    
    q->head = elem;
    
    if (q->head == NULL)
    {
        q->tail = q->head;
    }
    
    q->size--;
}

void Queue_RemoveElementById(Queue *q, int thread_id)
{
    // currently running queue:
    Node *previous = NULL;
    Node *head = q->head;

    while (head != NULL)
    {
        if (((thread *)head->value)->thread_id == thread_id)
        {
            if (previous == NULL)
            {
                q->head = head->next;
                
                if (q->head == NULL)
                {
                    q->tail = q->head;
                }
            }
            else
            {
                previous->next = head->next;
            }

            q->size--;

            free(head);
            
            break;
        }
        else
        {
            previous = head;
            head = head->next;
        }
    }

}