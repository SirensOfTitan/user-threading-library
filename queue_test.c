
#include <stdlib.h>
#include <stdio.h>
#include "queue.h"

void main()
{
    Queue *q = malloc(sizeof(Queue));
    q->head = q->tail = NULL;
    
    thread th;
    th.thread_id = 1;
    th.status = 1; 
    
    Queue_AddElement(q, &th);
    
    thread th2;
    th2.thread_id = 2;
    th2.status = 4;
    
    Queue_AddElement(q, &th2);
    Queue_RemoveElement(q);
    
    printf("Thread Top: %d", q->head->value->thread_id);
    
    
}
