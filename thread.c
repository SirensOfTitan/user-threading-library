/**
 * Author: Cole Potrocky
 */
 
#include <stdio.h>
#include <setjmp.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <syscall.h>
#include <stdlib.h>
#include "queue.h"
#include "thread.h"

/**
 * Flag used for UpdateSleepingThreads method.
 * Used to properly calculate sleep time upon
 * a CleanUp call.
 */
int cleanup = 0;

/**
 * Used to determine how many threads are suspended
 * (why I didn't call it suspended threads is 
 * unknown to me).
 */
int not_ready_threads = 0;
/**
 * Flag used as the non-inclusive maximum
 * valid index of the threads array.
 */
int thread_id_increment = 0;
/**
 * Keeps track of the number of threads
 * currently being used in the program
 * (i.e. all threads that do not have a DELETED
 *  status).
 */
int num_of_threads = 0;

struct itimerval tv;

/**
 * Holds a pointer to the currently running thread.
 */
thread *current_thread;

#ifndef LOTTERY
/**
 * Contains all threads that have a READY status,
 * stored in FIFO order to ensure proper Round Robin
 * treatment.
 */
Queue ready_threads;

#else
/**
 * Keeps track of the total number of tickets 
 * distributed to each of the threads.
 */
int num_of_tickets = 0;
#endif

/**
 * Contains pointers to all of the threads
 * being used.
 */
thread *threads[MAX_NO_OF_THREADS];


#ifdef __x86_64__
// code for 64 bit Intel arch

//typedef unsigned long address_t;
#define JB_SP 6
#define JB_PC 7

address_t translate_address(address_t addr)
{
    address_t ret;
    asm volatile("xor    %%fs:0x30,%0\n"
        "rol    $0x11,%0\n"
                 : "=g" (ret)
                 : "0" (addr));
    return ret;
}

#else

// code for 32 bit Intel arch
typedef unsigned int address_t;
#define JB_SP 4
#define JB_PC 5

address_t translate_address(address_t addr)
{
    address_t ret;
    asm volatile("xor    %%gs:0x18,%0\n"
        "rol    $0x9,%0\n"
                 : "=g" (ret)
                 : "0" (addr));
    return ret;
}

#endif

int CreateThread(void (*f)(void))
{
    if (MAX_NO_OF_THREADS - 1 < thread_id_increment)
    {
        return -1;
    }
    
    // Allocate a new thread struct
    thread *new_thread = malloc(sizeof(thread));
    
    // Allocate a stack for the thread
    new_thread->stack = malloc(STACK_SIZE * 1);
   
    // Setup thread information
    new_thread->thread_id = thread_id_increment;
    new_thread->status = READY;
    new_thread->function = f;
    new_thread->numOfBursts = 0;
    new_thread->secondsToSleep = -1;
    new_thread->hasArgs = 0;
    
    // Add thread to threads list:
    threads[thread_id_increment] = new_thread;  

#ifdef LOTTERY
    srand(time(NULL) + thread_id_increment);
    threads[thread_id_increment]->tickets = rand() % TICKET_SAMPLE_SIZE + 1;
    num_of_tickets += threads[thread_id_increment]->tickets;
#endif

    // Setup the jump data
    sigsetjmp(new_thread->jbuf, 1);
    (new_thread->jbuf->__jmpbuf)[JB_SP] = translate_address(
        ((address_t)new_thread->stack + STACK_SIZE - sizeof(address_t)));
    (new_thread->jbuf->__jmpbuf)[JB_PC] = translate_address((address_t)
        new_thread->function);
    sigemptyset(&(new_thread->jbuf->__saved_mask));

    thread_id_increment++;
    num_of_threads++;  
    
    return (new_thread->thread_id);
}

int CreateThreadWithArgs(void *(*f)(void *), void *arg)
{
    if (MAX_NO_OF_THREADS - 1 < thread_id_increment)
    {
        return -1;
    }

    // Allocate a new thread struct
    thread *new_thread = malloc(sizeof(thread));
    
    // Allocate a stack for the thread
    new_thread->stack = malloc(STACK_SIZE * 1);

    new_thread->hasArgs = 1;
   
    // Setup thread information
    new_thread->thread_id = thread_id_increment;
    new_thread->status = READY;
    new_thread->function = f;
    new_thread->functionArg = arg;
    new_thread->numOfBursts = 0;
    new_thread->secondsToSleep = -1;
    new_thread->hasArgs = 0;

    // Add thread to pending tasks
    // Add thread to threads list:
    threads[thread_id_increment] = new_thread;  

#ifdef LOTTERY
    srand(time(NULL) + thread_id_increment);
    threads[thread_id_increment]->tickets = rand() % TICKET_SAMPLE_SIZE + 1;
    num_of_tickets += threads[thread_id_increment]->tickets;
#endif

    address_t argAddr = translate_address((address_t)arg);

    if (sigsetjmp(new_thread->jbuf, 1) == 0)
    {
        (new_thread->jbuf->__jmpbuf)[JB_SP] = translate_address(
            ((address_t)new_thread->stack + STACK_SIZE - sizeof(address_t)));

        sigemptyset(&(new_thread->jbuf->__saved_mask));

        thread_id_increment++;
        num_of_threads++;  
    }
    else
    {
        current_thread->returnVal = (((void *(*)(void*)) current_thread->function)(current_thread->functionArg));
        struct itimerval tvNew;
        tvNew.it_value.tv_sec = 1;
        tvNew.it_value.tv_usec = 0;
        tvNew.it_interval.tv_sec = 1;
        tvNew.it_interval.tv_usec = 0;
        setitimer(ITIMER_VIRTUAL, &tvNew, NULL);

        current_thread->status = FINISHED;
#ifndef LOTTERY
        Queue_RemoveElementById(&ready_threads, current_thread->thread_id);
#endif
        num_of_threads--;

        Dispatch(SIGVTALRM);
    }
    
    return (new_thread->thread_id);
}

void UpdateSleepingThreads(void)
{
    clock_t clk = clock();
    // Update sleeping threads, if applicable
    //     before the scheduler runs.
    for (int i = 0; i < thread_id_increment; i++)
    {
        if (threads[i]->status == SLEEPING)
        {
            if ((clk - threads[i]->startSleepTime) >= (threads[i]->secondsToSleep * CLOCKS_PER_SEC)
                || cleanup == 1)
            {
                threads[i]->secondsToSleep = -1;
                threads[i]->status = READY;
            }
        }
    }
}

void Dispatch(int sig)
{
    if (not_ready_threads >= thread_id_increment)
    // The thread exchange is dead.
    {
        CleanUp();
    }

    if (current_thread != NULL)
    {
        current_thread->totalTime += (clock() - current_thread->recent_startTime);

        // Set the current thread as a candidate for
        //     scheduling
        if (current_thread->status == RUNNING)
            current_thread->status = READY;
    }

    UpdateSleepingThreads();

// Round-Robin Scheduler:
#ifndef LOTTERY 
    ScheduleThreads();
#endif

    if (current_thread != NULL)
    // Dispatch could be called while current_thread
    // does not have a context.
    {
        if(sigsetjmp(current_thread->jbuf, 1) == 0)
        {
            /**
             * Invalidate current_thread so that the
             * recorded running time does not go to
             * the last running thread (which would
             * happen in the case where all threads
             * are sleeping or suspended in some way)
             */
            current_thread = NULL;
            do
            {
                UpdateSleepingThreads();

#ifdef LOTTERY
                ScheduleThreads();
#else
                if (ready_threads.head != NULL)
                {
                    current_thread = (thread *)ready_threads.head->value;
                    Queue_RemoveElement(&ready_threads);
                }

                if (num_of_threads <= 0)
                {
                    CleanUp();
                }
#endif
            }
            while (current_thread == NULL || current_thread->status != READY);

                // if (current_thread == NULL)
                // {
                //     CleanUp();
                // }

            // Jump to function
            current_thread->status = RUNNING;
            current_thread->recent_startTime = clock();
            
            current_thread->numOfBursts += 1;
            siglongjmp(current_thread->jbuf, 1);
        }
        else
        {
            return;
        }
    }
}

int ScheduleThreads()
{
#ifndef LOTTERY
    // Add thread to queue if ready:
    for (int i = 0; i < thread_id_increment; i++)
    {
        if (threads[i]->status == READY)
            Queue_AddElement(&ready_threads, (void *)threads[i]);
    }

    return 1;
#else

    do
    {
        srand(clock());
        int random = rand() % num_of_tickets;
        int cumulative = 0;

        for (int i = 0; i < thread_id_increment; i++)
        {
            if (threads[i]->status == READY)
            {
                cumulative += threads[i]->tickets;
                if (random <= cumulative)
                {
                    current_thread = threads[i];

                    return 1;
                }
            }
        }
    }
    while (current_thread == NULL);
#endif
}

int GetMyId()
{
    return current_thread->thread_id;
}

int DeleteThread(int thread_id)
{
    if (thread_id >= 0 && thread_id < num_of_threads)
    {
        // Change thread status:
        int refThreadId = current_thread->thread_id;

        // In order to maintain proper array order
        // the thread is simply given a DELETED
        // status and its memory is deallocated
        // upon cleanup.
        threads[thread_id]->status = DELETED;
        num_of_threads--;
        not_ready_threads++;

        if (num_of_threads == 0)
        {
            CleanUp();
        }

        if (refThreadId == thread_id)
        // The thread is currently running.
        {
            // Allow dispatch to reschedule.
            // while (1) {}
            struct itimerval tvNew;
            tvNew.it_value.tv_sec = 1;
            tvNew.it_value.tv_usec = 0;
            tvNew.it_interval.tv_sec = 1;
            tvNew.it_interval.tv_usec = 0;
            setitimer(ITIMER_VIRTUAL, &tvNew, NULL);

            Dispatch(SIGVTALRM);

        }
#ifndef LOTTERY
        else
        {
            // The thread could still be in the
            Queue_RemoveElementById(&ready_threads, thread_id);
            return 1;
        }
#else
        return 1;
#endif
    }
}

void ToggleBlockThread(int thread_id, int status)
{
    threads[thread_id]->status = status;

    if (threads[thread_id]->status == BLOCKED)
    {
        Queue_RemoveElementById(&ready_threads, thread_id);
    }
    else
        Queue_AddElement(&ready_threads, threads[thread_id]);
}

void CleanUp()
{
    cleanup = 1;
    if (current_thread != NULL)
    {
        current_thread->totalTime += (clock() - current_thread->recent_startTime);
    }

    //UpdateSleepingThreads();

    // Shut down timer:
    struct itimerval tvNew;
    tvNew.it_value.tv_sec = 0;
    tvNew.it_value.tv_usec = 0;
    tvNew.it_interval.tv_sec = 0;
    tvNew.it_interval.tv_usec = 0;
    setitimer(ITIMER_VIRTUAL, &tvNew, NULL);

    for (int i = 0; i < thread_id_increment; i++)
    {
        float timeRunning = ((float)threads[i]->totalTime / 1000.0);
        double avgRunning = 0.0;
        double sleepTime = ((double)threads[i]->totalRequestedSleepTime * 1000.0);
        if (threads[i]->numOfBursts > 0)
        {
            avgRunning = ((double)threads[i]->totalTime / (threads[i]->numOfBursts * 1000));
        }

        printf("\n-------------------------\n");
        printf("Thread %d\n", i);
        printf("-------------------------\n");
        printf("Time Running: %f ms\n", timeRunning);
        printf("Number of Bursts: %d\n", (int)threads[i]->numOfBursts);
        printf("Average Time Quantum: %f ms\n", avgRunning);
        printf("Requested Sleep Time: %f ms\n", sleepTime);

        if (threads[i]->hasArgs == 1)
        {
            printf("Argument: %d\n", *((int*)threads[i]->functionArg));
        }

#ifdef LOTTERY
        printf("Tickets: %d\n", threads[i]->tickets);
#endif

        free(threads[i]->stack);
        free(threads[i]);
    }

    exit(0);
}

void Go()
{
    // printf("ALARM");
    signal(SIGVTALRM, Dispatch);
    
    tv.it_value.tv_sec = 1;
    tv.it_value.tv_usec = 0;
    tv.it_interval.tv_sec = 1;
    tv.it_interval.tv_usec = 0;

    setitimer(ITIMER_VIRTUAL, &tv, NULL);

    ScheduleThreads();

    // Setup the first current_thread
#ifndef LOTTERY
    if (ready_threads.head != NULL)
    {

        current_thread = (thread *)ready_threads.head->value;
        Queue_RemoveElement(&ready_threads);
#else
        ScheduleThreads();
#endif
        current_thread->recent_startTime = clock();
        current_thread->numOfBursts++;
        siglongjmp(current_thread->jbuf, 1);
#ifndef LOTTERY
    }
#endif

    // Keep this from returning:
    //while(num_of_threads > 0) {}
}

int SuspendThread(int thread_id)
{
    if (thread_id >= 0 && thread_id < thread_id_increment 
        && threads[thread_id]->status != DELETED)
    {
       threads[thread_id]->status = SUSPENDED;
       not_ready_threads++;
#ifndef LOTTERY
       Queue_RemoveElementById(&ready_threads, thread_id);
#endif

       if (thread_id == current_thread->thread_id)
       {
            struct itimerval tvNew;
            tvNew.it_value.tv_sec = 1;
            tvNew.it_value.tv_usec = 0;
            tvNew.it_interval.tv_sec = 1;
            tvNew.it_interval.tv_usec = 0;
            setitimer(ITIMER_VIRTUAL, &tvNew, NULL);

            Dispatch(SIGVTALRM);
       }

       return 0;
    }

    return -1;
}

#ifdef LOTTERY
void SetNumberOfTickets(int thread_id, int tickets)
{
    if (thread_id >= 0 && thread_id < thread_id_increment)
    {
        num_of_tickets -= threads[thread_id]->tickets;
        threads[thread_id]->tickets = tickets;
        num_of_tickets += tickets;
    }
}
#endif

void SleepThread(int sec)
{
    if (sec > 0)
    {
        current_thread->status = SLEEPING;
        current_thread->secondsToSleep = sec;
        current_thread->startSleepTime = clock();
        current_thread->totalRequestedSleepTime += sec;

        struct itimerval tvNew;
        tvNew.it_value.tv_sec = 1;
        tvNew.it_value.tv_usec = 0;
        tvNew.it_interval.tv_sec = 1;
        tvNew.it_interval.tv_usec = 0;
        setitimer(ITIMER_VIRTUAL, &tvNew, NULL);

        Dispatch(SIGVTALRM);
    }
}

void YieldCPU()
{
    struct itimerval tvNew;
    tvNew.it_value.tv_sec = 1;
    tvNew.it_value.tv_usec = 0;
    tvNew.it_interval.tv_sec = 1;
    tvNew.it_interval.tv_usec = 0;
    setitimer(ITIMER_VIRTUAL, &tvNew, NULL);

    Dispatch(SIGVTALRM);
}

int ResumeThread(int thread_id)
{
    if (thread_id >= 0 && thread_id < thread_id_increment 
        && threads[thread_id]->status == SUSPENDED)
    {
        threads[thread_id]->status = READY;
        not_ready_threads--;
#ifndef LOTTERY
        Queue_AddElement(&ready_threads, (void *)threads[thread_id]);
#endif
        return 0;
    }

    return -1;
}

int GetStatus(int thread_id, status *stat)
{
    if (thread_id >=0 && thread_id < thread_id_increment)
    {
        stat->thread_id = threads[thread_id]->thread_id;
        stat->current_status = threads[thread_id]->status;
        stat->totalTime = threads[thread_id]->totalTime;
        stat->totalRequestedSleepTime = threads[thread_id]->totalRequestedSleepTime;

        return (threads[thread_id]->thread_id);
    }

    return -1;
}