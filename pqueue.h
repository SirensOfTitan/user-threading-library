#ifndef PQUEUE
#define PQUEUE

#include <stdlib.h>
#include <setjmp.h>
#include <signal.h>
#include <time.h>
#include <stdio.h>

/**
 * Based on implementation at:
 * http://andreinc.net/2011/06/01/implementing-a-generic-priority-queue-in-c/
 */

typedef struct q
{
	size_t size; 
	size_t capacity;
	void **data;

	int (*cmp)(void *priorityOne, void *priorityTwo);
} PQueue;

PQueue *PQueue_New(int (*cmp)(void *p1, void *p2), size_t capacity);

void PQueue_Delete(PQueue *q);
void PQueue_Enqueue(PQueue *q, void *data);
void *PQueue_Dequeue(PQueue *q);

#endif
