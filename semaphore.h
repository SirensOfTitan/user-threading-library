/**
 * Author: Cole Potrocky
 */

#ifndef SEM
#define SEM

#include <stdio.h>
#include <setjmp.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <syscall.h>
#include <stdlib.h>
#include "thread.h"
#include "lock.h"
#include "pqueue.h"
#include "line.h"

typedef struct
{
	int value;
	mutex *m;
	PQueue *list;
} Semaphore;


/**
 * Initializes a semaphore to an initial value
 * of 'initial,' allocates memory for that
 * semaphore's priority queue, and initializes
 * the semaphore's mutex, which is used to 
 * ensure that Semaphore_Wait and Semaphore_Signal
 * are atomic.
 */
void Semaphore_Init(Semaphore *ref, int initial);

/**
 * Semaphore atomic wait function.  Second
 * passed parameter is the data structure stored
 * in the semaphore's priority queue.
 */
void Semaphore_Wait(Semaphore *ref, void *rep);

/**
 * Semaphore atomic signal function.
 */
void Semaphore_Signal(Semaphore *ref);


#endif
