/**
 * Author: Cole Potrocky
 */

#ifndef LOCK
#define LOCK

typedef volatile int mutex;

void Mutex_Lock(mutex*);
void Mutex_Unlock(mutex*);

#endif
