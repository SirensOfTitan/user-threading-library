/**
 * Author: Cole Potrocky
 */
 
#include <stdio.h>
#include <setjmp.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <stdlib.h>
#include "queue.h"
#include "thread.h"
#include "lock.h"
#include "semaphore.h"

int someN;

volatile int lock = 0;
Semaphore s;


#define SECOND 1000000

void t1(void)
{
    int j = 0;
    int i = 0;
    while (1)
    {
        printf("Thread %d: %d\n", GetMyId(), j);
        for (i = 0; i < 44445555; i++) // This would give some delay.
        {
        }

        /**
         * Testing Functions
         */

        // Yield CPU
        if (j == someN)
            YieldCPU();

        j++;
    }
}

void t2(void)
{
    int j = 0;
    int i = 0;
    while (1)
    {
        printf("Thread %d: %d\n", GetMyId(), j);
        for (i = 0; i < 44445555; i++) // This would give some delay.
        {
        }

        if (j == 3*someN)
        {
            SuspendThread(0);
            DeleteThread(2);
        }

        if (j == 5*someN)
            ResumeThread(0);

        if (j == 7*someN)
            SleepThread(someN / 2);

        if (j == (7*someN + 1))
            CleanUp();

        j++;
    }
}

void t3(void)
{
    int j = 0;
    while (1)
    {
        printf("apple\n");
        for (int i = 0; i < 44445555; i++) // This would give some delay.
        {
        }
    }
}

void t4(void)
{
    int j = 1;

    while(1)
    {
        if (j % 10 == 0)
            Semaphore_Signal(&s);
        printf("test\n");
        for (int i = 0; i < 44445555; i++) // This would give some delay.
        {
        }

        j++;
    }
}

void t5(void)
{
    int j = 0;
    while (1)
    {
        if (j <= 5)
            Semaphore_Wait(&s);
        printf("unlocked: %d\n", s.value);
        for (int i = 0; i < 44445555; i++) // This would give some delay.
        {
        }

        j++;
    }
}

int main(int argc, char *argv[])
{
    if (argc >= 2)
    {
        someN = atoi(argv[1]);
    }
    Semaphore_Init(&s, 4);

    CreateThread(t4);
    CreateThread(t5);
    // CreateThread(t2);
    // CreateThread(t3);

#ifdef LOTTERY
    SetNumberOfTickets(0, 1);
    SetNumberOfTickets(1, 2);
#endif 

    Go();

    return 0;
}