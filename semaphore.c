/**
 * Author: Cole Potrocky
 */
 
#include <stdio.h>
#include <setjmp.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <syscall.h>
#include <stdlib.h>
#include <stdbool.h>
#include "queue.h"
#include "thread.h"
#include "semaphore.h"
#include "lock.h"
#include "line.h"
#include "pqueue.h"

void Semaphore_Init(Semaphore *ref, int initial)
{
	mutex *m = malloc(sizeof(mutex));

	if (ref == NULL || m == NULL)
	{
		perror("Malloc allocation failed.");
		return;
	}

	ref->list = PQueue_New(compare, MAX_NO_OF_THREADS);
	ref->value = initial;
	ref->m = m;
	*(ref->m) = 0;
}

void Semaphore_Wait(Semaphore *ref, void *rep)
{
	Mutex_Lock(ref->m);
	ref->value--;

	if (ref->value < 0)
	{
		PQueue_Enqueue(ref->list, rep);
		ToggleBlockThread(GetMyId(), BLOCKED);

		if (((Person *)rep)->type == IST_TYPE)
		{
			setISTNum(getISTNum() + 1);
		}
		else
		{
			setCSENum(getCSENum() + 1);
		}

		Mutex_Unlock(ref->m);
		YieldCPU();
	}

	Mutex_Unlock(ref->m);
}

void Semaphore_Signal(Semaphore *ref)
{
	Mutex_Lock(ref->m);
	ref->value++;

	if (ref->value <= 0)
	{
		Person *temp = (Person *)
			PQueue_Dequeue(ref->list);

		if (temp->type == IST_TYPE)
		{
			setISTNum(getISTNum() - 1);
		}
		else
		{
			setCSENum(getCSENum() - 1);
		}

		if (temp != NULL)
			ToggleBlockThread(temp->thread_id, READY);
	}

	Mutex_Unlock(ref->m);
}