/**
 * Author: Cole Potrocky
 */

#ifndef LINE
#define LINE

#define IST_TYPE 1
#define CMP_TYPE 2

#define FAC 2
#define STUD 1

#define DOB 100000;
#define MAX_WAIT_TIME 999999999

typedef struct 
{
	int type;
	int status;
	int thread_id;
	// Unix Epoch time
	int dob;
} Person;


/**
 * Used to determine priority in the
 * priority queue.
 */
int compare(void *one, void *two);

/**
 * Getters for numbers of CSE, IST
 * people in line.
 */
int getISTNum();
int getCSENum();

/** 
 * Setters for numbers of CSE, IST
 * people in line.
 */
void setISTNum(int);
void setCSENum(int);


/**
 * Returns a random number with 
 * maximum size MAX_WEIGHT_TIME.
 *
 * Used to simulate variable 
 * traversal times leading up 
 * to and within critical path.
 */
int getWaitingTime();

/**
 * Runs a for loop to simulate variable
 * traversal times leading up to and within
 * critical path.
 */
void stroll();

/**
 * Sets up a Person data structure, which is 
 * used to simulate 'People' in the
 * CSE-IST Synchronization problem.
 */
void setupPerson(Person *ref, int type, int status, 
	int thread_id);

/**
 * Thread functions that represent the
 * different types of people in the
 * CSE-IST Synchronization problem.
 */
void cseFaculty();
void cseStudent();
void istPerson();

#endif